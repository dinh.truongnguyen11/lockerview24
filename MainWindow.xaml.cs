﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DemoDraw2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Locker previousOpenLocker = new Locker();
        private Locker currentOpenLocker = new Locker();
        List<Locker> listLockerPendings = new List<Locker>();

        const int COLOR_FORMAT_GREEN = 1;
        const int COLOR_FORMAT_GRAY = 2;
        const int COLOR_FORMAT_YELLO = 3;
        public MainWindow()
        {
            InitializeComponent();

            DrawLocker();


        }
        public class Locker : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;
            private int _color_format;
            public string locker_id { get; set; }
            public string name { get; set; }
            public int hardware_address { get; set; }
            public int size_id { get; set; }
            public int position { get; set; }
            public int width { get; set; }
            public int height { get; set; }
            public int group { get; set; }
            public int type { get; set; }
            public int status { get; set; }
            public int board_address { get; set; }
            public string block_name { get; set; }

            public int color_format
            {
                get => _color_format;
                set
                {
                    _color_format = value;
                    OnPropertyChanged("color_format");
                }
            }
            public DateTime recentTimeOpen { get; set; }
            public DateTime recentTimeClose { get; set; }
            public double totalPendingTime { get; set; }

            protected void OnPropertyChanged(string name)
            {
                if (PropertyChanged != null)
                {
                    PropertyChanged.Invoke(this, new PropertyChangedEventArgs(name));
                }
            }
        }

        public List<Locker> LockerOpenUIs { get; set; }
        public class BlockUI : ICloneable, IDisposable
        {
            public int block_id { get; set; }

            public int BlockWidth { get; set; }

            public List<Locker> LockerUIs { get; set; }


            public object Clone()
            {
                return base.MemberwiseClone();
            }
            // Other managed resource this class uses.
            private Component component = new Component();
            // Track whether Dispose has been called.
            private bool disposed = false;
            // Pointer to an external unmanaged resource.
            private IntPtr handle;
            public BlockUI()
            {
                this.handle = (IntPtr)System.Runtime.InteropServices.GCHandle.Alloc(this);
            }

            public void Dispose()
            {
                Dispose(true);
                // This object will be cleaned up by the Dispose method.
                // Therefore, you should call GC.SupressFinalize to
                // take this object off the finalization queue
                // and prevent finalization code for this object
                // from executing a second time.
                GC.SuppressFinalize(this);
            }
            protected virtual void Dispose(bool disposing)
            {
                // Check to see if Dispose has already been called.
                if (!this.disposed)
                {
                    // If disposing equals true, dispose all managed
                    // and unmanaged resources.
                    if (disposing)
                    {
                        // Dispose managed resources.
                        component.Dispose();
                    }

                    // Call the appropriate methods to clean up
                    // unmanaged resources here.
                    // If disposing is false,
                    // only the following code is executed.
                    CloseHandle(handle);
                    handle = IntPtr.Zero;

                    // Note disposing has been done.
                    disposed = true;

                    System.Diagnostics.Debug.WriteLine("Dispose BlockUI ~");
                }
            }
            // Use interop to call the method necessary
            // to clean up the unmanaged resource.
            [System.Runtime.InteropServices.DllImport("Kernel32")]
            private extern static Boolean CloseHandle(IntPtr handle);

            // Use C# destructor syntax for finalization code.
            // This destructor will run only if the Dispose method
            // does not get called.
            // It gives your base class the opportunity to finalize.
            // Do not provide destructors in types derived from this class.
            ~BlockUI()
            {
                // Do not re-create Dispose clean-up code here.
                // Calling Dispose(false) is optimal in terms of
                // readability and maintainability.
                Dispose(false);
            }
        }
        public List<BlockUI> BlockLockerUIs { get; set; }
        public List<Locker> GetDataLocker()
        {
            string currentDirectory = Directory.GetCurrentDirectory();
            string filePath = System.IO.Path.Combine(currentDirectory, "Data", "data.json");
            string json = File.ReadAllText(filePath);
            return JsonConvert.DeserializeObject<List<Locker>>(json).ToList();
        }
        private void DrawLocker(bool isRetireve = true)
        {
            try
            {
                var dataLocker = GetDataLocker();
                List<BlockUI> BlockUIs = new List<BlockUI>();

                BlockUIs = (from ll in dataLocker
                            group ll by ll.@group into ll
                            select new BlockUI { block_id = ll.Key, LockerUIs = ll.ToList() }).ToList();

                BlockUIs = BlockUIs.OrderBy(x => x.block_id).ToList();
                BlockUIs.ForEach(x => x.LockerUIs = x.LockerUIs.OrderBy(y => y.position).ToList());

                var countBlock = BlockUIs.Count();

                BlockLockerUIs = BlockUIs;

                foreach (var block in BlockLockerUIs.OrderBy(x => x.block_id))
                {
                    var lockerCount = block.LockerUIs.Count();
                    foreach (var locker in block.LockerUIs)
                    {
                        if (locker.status == 1)
                            locker.color_format = COLOR_FORMAT_GREEN;
                        else
                            locker.color_format = COLOR_FORMAT_GRAY;
                    }
                }

                LockerOpenUIs = dataLocker.FindAll(x => x.status == 1);


                // Bidding data to XAML
                this.DataContext = this;
            }
            catch (Exception ex)
            {
            }
            // End Loading page
        }
        private void NotiLockerOpen(Locker locker)
        {
            if (locker.color_format == COLOR_FORMAT_YELLO)
                return;
            tbLockerOpen.Text = locker.name;
            spNotiOpen.Visibility = locker.color_format == COLOR_FORMAT_GREEN ? Visibility.Visible : Visibility.Hidden;
        }

        private void CountTimerLockerPending()
        {
            if (currentOpenLocker.locker_id != null)
            {
                Locker currentLockerPending = listLockerPendings.Find(x => x.locker_id == currentOpenLocker.locker_id);
                if(currentLockerPending == null)
                {
                    currentOpenLocker.recentTimeOpen = DateTime.Now;
                    listLockerPendings.Add(currentOpenLocker);
                }
                else
                {
                    currentLockerPending.recentTimeOpen = DateTime.Now;
                }
            }

            if (previousOpenLocker.locker_id != null)
            {
                Locker previousLockerPending = new Locker();
                previousLockerPending = listLockerPendings.Find(x => x.locker_id == previousOpenLocker.locker_id);
                previousLockerPending.recentTimeClose = DateTime.Now;
                previousLockerPending.totalPendingTime += (previousLockerPending.recentTimeClose - previousLockerPending.recentTimeOpen).TotalSeconds;
            }
        }
        private void ChangeColorLocker(Locker locker)
        {
            if (locker.color_format == COLOR_FORMAT_YELLO || locker.color_format == COLOR_FORMAT_GREEN)
            {
                locker.color_format = COLOR_FORMAT_GRAY;
                currentOpenLocker = previousOpenLocker = new Locker();
                return;
            }

            CountTimerLockerPending();
            // locker = GRAY
            locker.color_format = COLOR_FORMAT_GREEN;
            currentOpenLocker.color_format = COLOR_FORMAT_YELLO;
            previousOpenLocker.color_format = COLOR_FORMAT_GRAY;
            previousOpenLocker = currentOpenLocker;
            currentOpenLocker = locker;
        }
        private void stpALocker_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            StackPanel stackPanel = sender as StackPanel;
            Locker locker = (Locker)stackPanel.Tag;

            if (locker.name.ToLower() == "screen")
                return;

            ChangeColorLocker(locker);
            NotiLockerOpen(locker);
        }
        private void borderHome_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            string message = "";
            listLockerPendings = listLockerPendings.OrderBy(x => x.name).ToList();
            foreach (var item in listLockerPendings)
            {
                if(item.totalPendingTime != 0)
                {
                    message += item.name + " -> " + item.totalPendingTime + Environment.NewLine;
                }
            }
            MessageBox.Show(message);
        }
    }

}
